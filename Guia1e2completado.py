#!/usr/bin/env python3
# -*- coding:utf-8 -*-


# Creacion clase libro


class Libro:
    def __init__(self):
        self.titulo = ""
        self.autor = ""
        self.ejemplares = 0
        self.prestados = 0

# Creacion get y set
    def gettitulo(self):
        return self.titulo

    def getautor(self):
        return self.autor

    def getejemplares(self):
        return self.ejemplares

    def getprestados(self):
        return self.prestados

    def settitulo(self, nuevotitulo):
        self.titulo = nuevotitulo

    def setautor(self, nuevoautor):
        self.autor = nuevoautor

    def setejemplares(self, nuevosejemplares):
        self.ejemplares = nuevosejemplares

    def setprestados(self, nuevoprestados):
        self.prestados = nuevoprestados

# Funcion para rellenar la biblioteca


def libros():
    libro = Libro()
    tit = input("Ingrese el titulo de libro: ")
    aut = input("Ingrese el autor: ")
    ejem = int(input("Ingrese el numero de ejemplares: "))
    pres = int(input("Ingrese el numero de prestados: "))
    libro.settitulo(tit)
    libro.setautor(aut)
    libro.setejemplares(ejem)
    libro.setprestados(pres)
    if pres > ejem:
        print("Valor no valido")
    else:
        print("Titulo: ", libro.getautor())
        print("Autor: ", libro.getautor())
        print("Ejemplares: ", libro.getejemplares())
        print("Ejemplares prestados: ", libro.getprestados())

    anadir = input("Desea añadir otro libro? Pulse Y para si")
    if anadir == "Y":
        return libros()
    else:
        print("Adios!")

libros()
