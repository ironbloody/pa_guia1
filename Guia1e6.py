#!/usr/bin/env python3
# -*- coding:utf-8 -*-


class Cuenta:

    def __init__(self):
        self.nombredelcliente = ""
        self.numerodecuenta = 0
        self.saldo = 0

    def getnombredelcliente(self):
        return self.nombredelcliente

    def getnumerodecuenta(self):
        return self.numerodecuenta

    def getsaldo(self):
        return self.saldo

    def setnombredelcliente(self, nuevonombredelcliente):
        self.nombredelcliente = nuevonombredelcliente

    def setnumerodecuenta(self, nuevonumerodecuenta):
        self.numerodecuenta = nuevonumerodecuenta

    def setsaldo(self, nuevosaldo):
        self.saldo = nuevosaldo

cuenta = Cuenta()
Name = input("Ingrese el nombre del cliente: ")
Account = int(input("Ingrese el numero de la cuenta: "))
Balance = int(input("Ingrese el saldo de la cuenta: "))
cuenta.setnombredelcliente(Name)
cuenta.setnumerodecuenta(Account)
cuenta.setsaldo(Balance)
print("\n")
Deposito = input("¿Desea depositarse? Pulse Y para si y cualquier otro boton para no: ")
if Deposito == "Y":
    Newbalance = int(input("¿Cuanto desea depositar?"))
    sumbalance = Balance + Newbalance
    if Newbalance <= 0:
        print("Valor no valido")
        print(Newbalance >= 0)
    else:
        cuenta.setsaldo(sumbalance)
        print(sumbalance > 0)
print("\n")
Giro = input("¿Desea girar? Pulse Y para si y cualquier otro boton para no: ")
if Giro == "Y":
    Newbalance2 = int(input("¿Cuanto desea girar?"))
    sumbalance2 = (Newbalance2 - Balance) * -1
    if Newbalance2 <= 0:
        print("Valor no valido")
        print(Newbalance2 >= 0)
    if Newbalance2 > cuenta.getsaldo():
        print("No tiene saldo suficiente")
        print(Newbalance2 < cuenta.getsaldo())
    else:
        cuenta.setsaldo(sumbalance2)
        print(sumbalance2 >= 0)
print("\n")
Transferencia = input("¿Desea realizar una transferencia? Pulse Y para si y cualquier otro boton para no: ")
if Transferencia == "Y":
    numerocuenta2 = int(input("¿Cual es el numero de cuenta?"))
    cuenta.setnumerodecuenta(numerocuenta2)
    Newbalance3 = int(input("¿Cuanto desea girar?"))
    sumbalance3 = (Newbalance3 - Balance) * -1
    if Newbalance3 <= 0:
        print("Valor no valido")
    if Newbalance3 > cuenta.getsaldo():
        print("No tiene saldo suficiente")
    else:
        cuenta.setsaldo(sumbalance3)
        print("Numero de cuenta transferida: ", cuenta.getnumerodecuenta())
        print("Nombre del cliente: ", cuenta.getnombredelcliente())
        print("Numero de la cuenta: ", cuenta.getnumerodecuenta())
        print("Saldo de la cuenta:", cuenta.getsaldo())
else:
    print("\n")
    print("Nombre del cliente: ", cuenta.getnombredelcliente())
    print("Numero de la cuenta: ", cuenta.getnumerodecuenta())
    print("Saldo de la cuenta:", cuenta.getsaldo())