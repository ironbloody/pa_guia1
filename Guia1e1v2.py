#!/usr/bin/env python3
# -*- coding:utf-8 -*-


class tarjeta:
    def __init__(self):
        self.nombre = ""
        self.saldo = 0
        self.descuento = 0

    def getnombre(self):
        return self.nombre

    def getsaldo(self):
        return self.saldo

    def getdescuento(self):
        return self.descuento

    def setnombre(self, nuevonombre):
        self.nombre = nuevonombre

    def setsaldo(self, nuevosaldo):
        self.saldo = nuevosaldo

tarjet = tarjeta()
name = input("Ingrese el nombre del titular: ")
tarjet.setnombre(name)
balance = int(input("Ingrese el saldo de la tarjeta: "))
tarjet.setsaldo(balance)
discount = int(input("Ingrese el valor de la compra: "))
print(tarjet.getnombre(), ",", tarjet.getsaldo(), "$")
if discount > balance:
    print("No tienes saldo suficiente")
else:
    newbalance = balance-discount
    tarjet.setsaldo(newbalance)
    print("El nuevo saldo es: ", tarjet.getsaldo(), "$")
