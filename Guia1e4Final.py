#!/usr/bin/env python3
# -*- coding:utf-8 -*-

# Creacion clase amigos y sus metodos


class Amigos:
    def _init_(self):
        self.__disponible = 0
        self.amigos = 0
        self.valorcompra = 0
# Creacion de los set y get

    def getdisponible(self):
        return self.disponible

    def getamigos(self):
        return self.amigos

    def getvalorcompra(self):
        return self.valorcompra

    def setdisponible(self, nuevodisponible):
        self.disponible = nuevodisponible

    def setamigos(self, nuevosamigos):
        self.amigos = nuevosamigos

    def setvalorcompra(self, nuevovalorcompra):
        self.valorcompra = nuevovalorcompra

amigos = Amigos()
friends = int(input("Cuantos amigos son? "))
newfriends = friends + 1
amigos.setamigos(newfriends)
balance = int(input("Cuanto saldo tienen disponible? "))
amigos.setdisponible(balance)
buy = int(input("Cual es el valor de la compra?"))
amigos.setvalorcompra(buy)
print("Saldo disponible: ", (amigos.getdisponible()), "$")
print(amigos.getamigos(), "Amigos")
print("El valor de la compra es: ", amigos.getvalorcompra(), "$")
if buy > balance:
    print("No tienen suficiente dinero")

else:
    """Condicion para calcular el valor de la
        compra sin contar el IVA y propina"""

    IVA = buy * 0.19
    propina = buy * 0.10
    totalbalance = buy + IVA + propina
    amigos.setvalorcompra(totalbalance)
    print("El valor de la compra sin IVA y propina es: ",
          amigos.getvalorcompra(), "$")
    pagar = (totalbalance / newfriends)
    if totalbalance > balance:
        print("No alcanza la plata :c")
    else:
        amigos.setdisponible(pagar)
        print("Cada uno debe pagar: ", amigos.getdisponible(), "$")
